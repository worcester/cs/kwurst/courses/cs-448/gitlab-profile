# Set-up Tasks

## Objectives

The objective of this assignment is for students to get themselves set up
with some of the tools and websites that they will need for assignments
during the semester.

Completing all five of the tasks by the appropriate due dates will let you
start the semester with 5 Tokens.

***Note: All of these tasks will have to be done at some point in the
semester anyway to be able to complete a particular assignment in the
course. So you might as well do it now, and earn a Token.***

## Task 1: Complete Team Formation Questionnaire

### *Worth 1 Token*

### Introduction

We will be working in teams throughout the semester. Each team will be
working in
[Thea's Pantry](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry).

There will be 7 projects this semester. I would like to give you some input
into what project you will work on.

In addition, it will be helpful for each team to have members who can work
with a number of different areas/skills/technologies (API, Backend, Frontend,
User Experience (UX), Infrastructure, Testing, Documentation, Team Management).

Finally, I would like to give you the chance to work with others that you
work well with.

In an attempt to make sure each team has people who have some interest in
the project assigned to the team, who have the needed areas/skills/technologies,
and who work well together, I have created a questionnaire to help me form
the teams.

It will not be possible for me to assign everyone to their first choice of
team and to work with their first choice of teammates (if for no other reason
than the teams are split between two sections). But, I will try to take your
preferences into account when assigning you to a team.

### Complete the Team Formation Questionnaire

Complete the [Team Formation Questionnaire](https://forms.gle/Zu8f1rp9CrgHem2n7).

I need this completed by the end of the week before classes begin, so I can
analyze the results and form teams for the first class.

### Due Date

By Friday, 17 January 2025 - 23:59

## Task 2: Submit GitLab Username

### *Worth 1 Token*

### Introduction

We will be using GitLab.com for working on our code in teams for
LibreFoodPantry.

### Create a GitLab.com Account

As GitLab.com is a cloud service outside of WSU, you must create your own
account, and inform me of the username so that I can add you to the team for
the course.

If you do not already have an account on GitLab.com, go to 
[https://gitlab.com/users/sign_in#register-pane](https://gitlab.com/users/sign_in#register-pane),
and create an account.

### Submit your GitLab Username

Submit your GitLab username through this
[Google Form](https://forms.gle/2uq4V5c4bPv6bw339)

### Due Date

By 17 January 2025 - 23:59

## Task 3: Install Git, Docker, and Visual Studio Code

### *Worth 1 Token*

### Introduction

We will be using Dev Containers through Visual Studio Code to ensure that
everyone has a consistent development environment. To do that you will need
to have Git, Docker Desktop, and VSCode installed on your laptop to bring to
class.

This installation is not always trivial, and we need these tools installed
before the first class, so you will have to do the installations on your own
before the first class.

### Install

Follow the instructions here to do the installations:
[https://gitlab.com/worcester/cs/kwurst/software-development-supplemental-materials/-/blob/main/Installing-Git-Docker-VSCode.md?ref_type=heads](https://gitlab.com/worcester/cs/kwurst/software-development-supplemental-materials/-/blob/main/Installing-Git-Docker-VSCode.md?ref_type=heads)

Because this involves updating your OS and installing Docker Desktop (which may require changes to system settings and multiple reboots) this is not something that you should wait until the last minute to do.

### Submit proof that you have a working development environment

#### Clone the GuestInfoBackend to Your Own Computer and Open in VSCode

 1. Go to the [GuestInfoBackend project](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend)
 2. From the blue  `Code v`  button, choose `Visual Studio Code (HTTPS)`.

     If your browser asks, allow it to open the link in Visual Studio Code.
 3. Choose the folder on your computer where you want the project saved. (You may want to create a folder that you use for all projects for this class to make it easier to find again later.)
 4. Enter your GitLab username.
 5. Enter your GitLab password.
 
     If VScode asks, let it know that you trust the authors of the files.

#### Reopen the Project in a Container

1. Make sure that Docker Desktop is running on your computer.
2. Click `Reopen in Container` in the pop-up in VS Code.

    If the pop-up has closed before you are able to click on it:

     1. Click on the `Open in a Remote Window button  ><`  in the bottom left.
     2. Choose `Reopen in Container`.
3. Wait until  `>< Dev Container: Node.js @ desktop-linux`  appears in the bottom left. This may take a while the first time you do this on a computer where this has not been done before. It will be faster after the first time on this same computer.

#### Submit a screenshot to Blackboard

Take a screen shot that includes the left-hand side of your VSCode window, showing the list of files and the bottom left showing `>< Dev Container: Node.js @ desktop-linux`. Upload the screenshot in Blackboard under Content > Submit Set-up Task #3.

### Due Date

By 20 January 2025 - 23:59

## Task 4: Join the class and LibreFoodPantry Discord servers

### *Worth 1 Token*

### Introduction

We will be working in Discord during class time, and outside of class.

### Join the class and LFP Discord servers

1. Join the [class Discord server](https://discord.gg/Sd4djaktAG)
2. Join the [LibreFoodPantry Discord server](https://discord.gg/PRth8YK).

### Deliverable Specification

Your assignment must meet *all* of the following specifications by the due date to earn 1 token.

* Your username on the class Discord server must include your first and last name.

### Due Date

By 20 January 2025 - 23:59

## Task 5: Familiarize Yourself with LibreFoodPantry and Thea's Pantry

### *Worth 1 Token*

### Introduction

The LibreFoodPantry community has information about our goals and how we work on our website [https://librefoodpantry.org](https://librefoodpantry.org). The information about Thea's Pantry is in GitLab: [https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry).

### Read the Items Linked from the LibreFoodPantry Main Page

Read the items linked from the website [https://librefoodpantry.org](https://librefoodpantry.org).
* Mission
* Values (including Agile Values and Principles, and FOSSisms)
* Code of Conduct
* Licensing
* Acknowledgements
* Coordinating Committee

### Explore the Thea's Pantry GitLab Group

* Explore the groups for the various subsystems for Thea's Pantry
* In the Documentation repository, read the files linked from the README.md file:
  * User Stories
  * Architecture
  * Technology
  * Workflow
  * Release Process

### Deliverable Specification

Your assignment must meet *all* of the following specifications by the due date to earn 1 token.

A blog post about your reading of the items linked to the LibreFoodPantry main page and the items from Thea's Pantry.

* Choose one thing from LibreFoodPantry you found interesting, surprising, or useful from the reading.
  * What was it about?
  * Why did you choose to write about it?
* Choose one thing from Thea's Pantry you found interesting, surprising, or useful from the reading.
  * What was it about?
  * Why did you choose to write about it?
* Tag it with `CS@Worcester`, `CS-448`, and `Set-up Task #5`.
* 200-300 words.

Your blog post must appear on the [CS@Worcester blog](http://cs.worcester.edu/).
You should have this set up from a previous semester. Check to see that your
blog post shows up on the [CS@Worcester blog](http://cs.worcester.edu/). You
may have to wait up to 1 hour for it to appear. If it does not appear after
1 hour, email me the link to your blog post.

### Due Date

* Sunday, 26 January 2025 - 23:59

&copy; 2025 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA
