# Working Agreements

## Background

### Have you ever worked in a team where one or more team members…

* is always late or misses meetings?
* do not complete their work, on time?
* are rude or argue in a meeting?
* do not participate in meetings?
* take over the meeting?

### Working Agreements Can Help

* Turns out that there is help!
* Working Agreement: set of guidelines that team members agree to follow
  in order to improve communication and collaboration.
* Created by the Team,
  * For the Team,
  * And establishes expectations of the Team for one another.

### Why do we need a Working Agreement?

* Team members have different work styles and different expectations which
  often don’t match
* Working agreements:
  * Create shared sense of responsibility and
    individual sense of ownership
  * Make positive and negative behavior clear
  * Provide ways to keep team accountable
  *Improve team productivity

### Characteristics of a Working Agreement

* Collaborative – created by the team
* Publicly visible – posted in a common space
* Short – items need to be easily remembered
* Consequential – clearly spells out
  consequences when agreement is violated
* Frequently updated – changed to fit the project and team

### Benefits of a Working Agreement

* Creates common understanding of how team will work
* Makes team roles and responsibilities clear
* Manages expectations
* Facilitates communication
* Builds trust

## Activity

### Team Experience (10 minutes)

Each team member should share a positive team experience - an experience
in which they worked in a successful team. What were the characteristics
of the team? What positive attributes come to mind when you consider that
team? Make sure that every team member shares.

List these attributes in a document.

### Health Signals (8 minutes)

Now identify the characteristics that you will use to identify that your
team is operating well. What are some of the things that you should see?
These ideas should be specific. For instance, “good collaboration” is not
quantifiable. What does “good collaboration” look like? Does this mean that
sub-teams will meet regularly? That each artifact developed by the team will
be reviewed by at least one other team member? Pair-programming? What else?

List these characteristics in your document.

### Psychological Safety (8 minutes)

Identify how the team is going to create an environment that is
psychologically safe for everyone on the team.

>“Psychological safety is the belief that you won’t be punished or
>humiliated for speaking up with ideas, questions, concerns, or mistakes.
>At work, it’s a shared expectation held by members of a team that
>teammates will not embarrass, reject, or punish them for sharing ideas,
>taking risks, or soliciting feedback.” 
>
>(https://www.ccl.org/articles/leading-effectively-articles/what-is-psychological-safety-at-work/)

How will you ensure that everyone in your team feels able to freely speak up?

Describe in your document.

### Accountability (10 minutes)

Describe how the team will hold each other accountable. Each team member
should share how they would like to be held accountable and spoken to when
mistakes happen. In your document, describe how each team member should be
approached if they have let another team member down.

## Creating a Working Agreement

When people work together, they must come to some agreement about how they
will work together including how interactions will happen and guidelines for
the work. In order to get the team on the same page and improve collaboration,
it makes sense for the guidelines of how the team will operate be decided
collaboratively by the entire team and written down to make such guidelines
plain.

In this part of the assignment, you must create a Working Agreement
for your team.

You must:

* Go to your team's are in the class Discord server.
* Create a new message in the `#general` channel.
  * Start with the line `**Working Agreement**`
  * Use `Shift+Enter` whenever you want to go to the next line without
    posting the message.
  * Include all of the sections defined below in this Working Agreement
    * You can use `-` to create bullets items for each section, and two spaces
      (not tabs) followed by `-` to indent bullets inside each section.
  * Make sure that the message has no spelling or formatting errors.
  * Pin this message to the top of your `#general` channel so anyone on the
    team can find the Working Agreement easily when needed.
  * Turn the message into a thread if you want to post suggestions for future
    changes to your Working Agreement. Discuss the proposed change(s) and 
    agree as a team on changes to accept, then edit your original message

### Sections

Your Working Agreement must contain at least the following sections, in the
order listed below. You should have sufficient bullets under each section in
order to fully cover the topic. Some sections may have more bullets than
others. At the end of this document are some items to seed your thinking.
This is in no way a complete list.

* Culture: In this section, you will define the culture of your team.
  Provide 2-4 positive characteristics that all team members should bring
  to all work.
* Meetings: Describe your meeting times. Where will meetings take place?
  You should take notes in your meetings to document decisions and actions.
  Where will meeting notes be stored? Will your meeting have a format? If so,
  what is it? Team meetings result in work items. How will the team follow
  up on these work items? Describe the protocol for letting team members
  know if you cannot make a meeting which should happen very rarely if ever.
  Describe consequences of missing or late attendance at meetings.
* Communication: Describe communication channels you will use. How will you
  communicate? Describe the expected communication pattern. Will people be
  required to respond within a particular time-frame?
* Decisions/Conflict: Describe how decisions will be made. How will “ties”
  be decided? How will conflicts within the team be handled?
* Development: In this section, describe how you will coordinate work to be
  completed. How will work be allocated? How will work be completed? I
  understand that you may not completely understand how the project will be
  developed. We will revisit this section after we have gotten into the
  actual project. Describe the consequences of work that is not completed
  or completed incorrectly.

## Examples

### Proposing changes

Everyone in the team can propose changes to our working agreements.

You can propose changes to this document by adding a comment or suggestion.
You can also always suggest new working agreements in the team retrospectives.

### Culture

As a team, we agree to…

* Ask for help when we need it
* Be transparent and honest
* Actively give constructive feedback
* Be respectful
  * When another Team member is talking, don’t interrupt.
  * When they’ve finished, pause, reflect, and share back your understanding
  of their idea.
* Make sure everyone has a voice
  * More vocal Team members volunteer to speak last to give quieter Team
  members an opportunity to speak up.

### Communication & Meetings

As a team, we agree to…

* Keep project related discussion on the Team's Discord channels or in
  GitHub PR comments for searchability.
* Discuss in Discord threads instead of the main channel, if possible.
* Attend a daily meeting every weekday at 9:45-10:00
  * Agenda: Go issue tracker status, discuss any blockers
* Attend a weekly meeting every Tuesday at 10:00-11:00
  * Agenda: Demos, check current progress
* Attend retrospectives
* Send calendar invitations for all team meetings in advance
* Communicate our schedule and mark absences into the work calendar
* Join meetings on time
* Cellphones in Team Events stay put away.
* Dissent must be respectful.
  * To improve the quality of all ideas, we encourage respectful dissent.
  * At least one Team member volunteers to be the dissenter.
    Even the original idea is good, they’re expected to find
    criticisms of the idea with the goal of making it stronger.
* Team is focused on the Sprint Goal.
  * If something comes up that doesn’t fit the goal, Team Members are
  expected to say no.

### Development

As a team, we agree to…

* Review pull requests in less than 1 day
* Definition of done - As a team, we agree that a task is done, when…
  * The change has sufficient automated tests
  * Changes have been tested on the staging environment
  * Code has been peer-reviewed
  * All continuous integration (CI) checks pass
  * Code is merged to the main branch and deployed successfully
  * Relevant documentation is updated
  * Task goal met

Based on materials from Professor Heidi Ellis at Western New England
University, Springfield, MA.

&copy; 2025 Karl R. Wurst and Heidi Ellis

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA2019